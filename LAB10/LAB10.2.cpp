#include <iostream>
#include <string>

using namespace std;

class Vehicle
{
protected:
	int n_wheels;
	float engine_capacity;
	string engine_type;
	string name;
public:
	// Remember that virtual classes have NO CONSTRUCTOR !
	virtual ~Vehicle()
	{
	}
	virtual void engineStart() = 0;
	virtual void engineStop() = 0;
	virtual void displayStats() = 0;
	string vehicleName()
	{
		return name;
	}
};

class Car : public Vehicle
{
private:
	float trunk_capacity;
public:
	Car(int n_w, float e_c, string e_t, string n, float t_c)
		: trunk_capacity(t_c)
	{
		n_wheels = n_w;
		engine_capacity = e_c;
		engine_type = e_t;
		name = n;
		cout << "This is Car constructor!" << endl;
	}
	
	~Car()
	{
		cout << "Car destroyed" << endl;
	}

	virtual void engineStart() override
	{
		cout << "Checking engine..." << endl;
		cout << engine_capacity << "L " << engine_type << 
			" roars loudly!"<<endl;
	}

	virtual void engineStop() override
	{
		cout << engine_type << " modestly stops" << endl;
	}

	virtual void displayStats() override
	{
		cout << "Car name: " << name << endl;
		cout << "Engine type: " << engine_type << 
			", capacity: "<< engine_capacity <<"L"<<endl;
		cout << "Trunk capacity: " << trunk_capacity << endl;
		cout << "This vehicle has " << n_wheels << " wheels" << endl;
	}

	void carName()
	{
		cout << name << endl;
	}
};

class Motorbike : public Vehicle
{
public:
	Motorbike(int n_w, float e_c, string e_t, string n)
	{
		n_wheels = n_w;
		engine_capacity = e_c;
		engine_type = e_t;
		name = n;
	}
	~Motorbike()
	{
		cout << "Motorbike is destroyed!" << endl;
	}
	virtual void engineStart() override
	{
		cout << " Engine roars loudly!" << endl;
	}
	virtual void engineStop() override
	{
		cout << "Engine modestly stops" << endl;
	}
	virtual void displayStats() override
	{
		cout << "Bike name: " << name << endl;
		cout << "Engine type: " << engine_type << ", capacity:"
			<< engine_capacity << "L" <<endl;
		cout << "This vehicle has " << n_wheels << " wheels" << endl;
	}
	string bike_Name() {
		return name;
	}
};

int main() {
	Car camaro(4, 6.5, "L78 V8", "1969 Camaro SS 396�", 4);
	Motorbike rocket(2, 2.3,
		"In-line three, four-stroke", 
		"Triumph Rocket III Roadster");
	Vehicle *ptr = nullptr;
	int a = 0;

	do
	{
		cout << "Pick your ride: " << endl <<
			"1. " << camaro.vehicleName() << "." << endl;
		cout << "2. " << rocket.vehicleName() << "." << endl;
		cin >> a;
	} while (a != 1 && a != 2);

	if (a == 1)
	{
		ptr = &camaro;
	}
	else if(a == 2)
	{
		ptr = &rocket;
	}
	cout << "You have chosen your ride!" << endl;
	ptr->displayStats();
	ptr->engineStart();
	cout << "Shame it�s just a virual vehicle :(" << endl;
	ptr->engineStop();
	system("pause");
}