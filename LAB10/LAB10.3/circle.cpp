#include<iostream>
#include<math.h>
#include"circle.h"
using namespace std;

const float pi = 3.14159;

Circle::Circle(double r)
	: radius(r)
{
}

Circle::~Circle()
{
	cout << "Circle was destroyed!" << endl;
}

double Circle::area()
{
	return pi * pow(radius, 2);
}

double Circle::circumference()
{
	return 2 * pi * radius;
}

