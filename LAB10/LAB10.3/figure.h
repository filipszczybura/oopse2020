#ifndef FIGURE_H
#define FIGURE_H
#include <iostream>
#include <string>

using namespace std;

class Figure
{
public:
	virtual double area() = 0;
	virtual double circumference() = 0;
	virtual ~Figure()
	{

	}
};
#endif