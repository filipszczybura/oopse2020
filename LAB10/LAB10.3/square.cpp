#include<iostream>
#include<math.h>
#include"square.h"
using namespace std;

Square::Square(double a)
	: sideA(a)
{

}

Square::~Square()
{
	cout << "Square was destroyed!" << endl;
}

double Square::area()
{
	return pow(sideA, 2);
}

double Square::circumference()
{
	return 2 * sideA;
}
