#ifndef RECTANGLE_H
#define RECTANGLE_H

#include<iostream>
#include"figure.h"

using namespace std;

class Rectangle : public Figure
{
private:
	double sideA, sideB;
public:
	Rectangle(double, double);
	~Rectangle();
	virtual double area() override;
	virtual double circumference() override;
};

#endif
