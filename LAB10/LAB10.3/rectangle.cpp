#include<iostream>
#include<math.h>
#include"rectangle.h"
using namespace std;

Rectangle::Rectangle(double a, double b)
	: sideA(a), sideB(b)
{
}

Rectangle::~Rectangle()
{
	cout << "Rectangle was destroyed!" << endl;
}

double Rectangle::area()
{
	return sideA * sideB;
}

double Rectangle::circumference()
{
	return sideA + sideB;
}