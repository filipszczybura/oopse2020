#ifndef CIRCLE_H
#define CIRCLE_H

#include<iostream>
#include"figure.h"

using namespace std;

class Circle : public Figure
{
private:
	double radius;
public:
	Circle(double);
	~Circle();
	virtual double area() override;
	virtual double circumference() override;
};

#endif
