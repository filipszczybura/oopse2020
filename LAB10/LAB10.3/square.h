#ifndef SQUARE_H
#define SQUARE_H

#include<iostream>
#include"figure.h"

using namespace std;

class Square : public Figure
{
private:
	double sideA;
public:
	Square(double);
	~Square();
	virtual double area() override;
	virtual double circumference() override;
};

#endif
