#include <iostream>
#include <string>
#include<math.h>

#include"figure.h"
#include"circle.h"
#include"rectangle.h"
#include"square.h"

using namespace std;

int main() {
	int user_input;
	do
	{
		cout << "\t\t\t\tChoose operation:" << endl <<
			"1. Calculate area of the circle.\n" <<
			"2. Calculate circumference of the circle.\n" <<
			"3. Calculate area of the rectangle.\n" <<
			"4. Calculate circumference of the rectangle.\n" <<
			"5. Calculate area of the square.\n" <<
			"6. Calculate circumference of the square.\n" <<
			"0. Exit" << endl;

		cin >> user_input;
		if (user_input > 6 || user_input < 0)
		{
			cout << "Learn how to read and " <<
				"then choose one of the following options: " << endl;
		}
		else
		{
			switch (user_input)
			{
			case 0:
			{
				cout << "Ending program" << endl;
				break;
			}
			break;
			case 1:
			{
				double r;
				cout << "What is the radius: ";
				cin >> r;
				Circle c(r);
				cout << "Area of the circle is: " <<
					c.area() << endl;
			}
			break;
			case 2:
			{
				double r;
				cout << "What is the radius: ";
				cin >> r;
				Circle c(r);
				cout << "Circumference of the circle is: " <<
					c.circumference() << endl;
			}
			break;
			case 3:
			{
				double a, b;
				cout << "How long is the side a? ";
				cin >> a;
				cout << "How long is the side b? ";
				cin >> b;
				Rectangle r(a, b);
				cout << "Area of the rectangle is " <<
					r.area() << endl;
			}
			break;
			case 4:
			{
				double a, b;
				cout << "How long is the side a? ";
				cin >> a;
				cout << "How long is the side b? ";
				cin >> b;
				Rectangle r(a, b);
				cout << "Circumference of the rectangle is " <<
					r.circumference() << endl;
			}
			break;
			case 5:
			{
				double a;
				cout << "How long is the side? ";
				cin >> a;
				Square s(a);
				cout << "Area of the square is " <<
					s.area() << endl;
			}
			break;
			case 6:
			{
				double a;
				cout << "How long is the side? ";
				cin >> a;
				Square s(a);
				cout << "Circumference of the square is " <<
					s.circumference() << endl;
			}
			break;
			}
		}
	} while (user_input != 0);

	system("pause");
}