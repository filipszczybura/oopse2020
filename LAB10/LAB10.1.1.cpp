#include <iostream>

using namespace std;

class Base
{
public:
	void hat()
	{
		cout << "This is an empty hat" << endl;
	}
};

class Derived : public Base
{
public:
	void hat()
	{
		cout << "Hat with a magical rabbit inside!" << endl;
	}
};

int main() {
	auto base_ptr = make_unique<Base>();
	auto derived_ptr = make_unique<Derived>();
	base_ptr->hat();
	derived_ptr->hat();
	system("pause");
}