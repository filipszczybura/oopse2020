#include <iostream>


using namespace std;

class Door; //declaration of the class (neccessary to work ! like it is a global variable which is needed for main to work !!!! )

class Home
{
private:
	bool flowerWater;
public:
	
	Home(bool water)
	{
		flowerWater = water;
	}

	void dayPassed()
	{
		flowerWater = false;
		cout << "Day has passed , flower needs water" << endl;
	}

	void owner()
	{
		flowerWater = true;
		cout << "Flower has been hydrated by the house owner" << endl;
	}

	friend void gardener(Home& h, Door& d);
};

class Door
{
	bool lock;
public:

	Door(bool state)
	{
		lock = state;
	}

	void doorOpen()
	{
		lock = false;
		cout << "Door has been opened" << endl;
	}
	void doorClosed()
	{
		lock = true;
		cout << "Door has been closed" << endl;
	}
friend void gardener(Home& h, Door& d);
};

void gardener(Home& h, Door& d)
{
	d.doorOpen();
	h.flowerWater = true;
	cout << "Flower has been hydrated by the gardener" << endl;
	d.doorClosed();
}


int main()
{
	Home h1(false);
	Door d1(true);

	h1.owner();
	cout << "Owner: it is time for adventure! Gardener will take care of my flower!" << endl;
	d1.doorOpen();
	cout << "Owner left" << endl;
	d1.doorClosed();
	h1.dayPassed();
	gardener(h1, d1);

	system("pause");
}