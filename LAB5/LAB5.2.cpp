#include <iostream>
#include<string>

using namespace std;

typedef float realNumber; // defines new data type realNumber
typedef double overloadedRealNumber;

int main()
{
	realNumber number1 = 4.5;
	realNumber number2 = 3566789.234576;
	overloadedRealNumber number3 = 698519801234.38254;
	overloadedRealNumber number4 = 3566789.234576;

	cout << "a = " << number1 << endl;
	cout << "b = " << number2 << endl;
	cout << "c = " << number3 << endl;
	cout << "d = " << number4 << endl;

	system("pause");
}