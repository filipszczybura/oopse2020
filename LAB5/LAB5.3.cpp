#include <iostream>
#include<string>

using namespace std;

int area(int a)
{
	return pow(a, 2);
}

int area(int a, int b)
{
	return a * b;
}

int main()
{
	int lengthA = 2, lengthB = 4;
	cout << "There is a rectangle in which two squares are inscribed" << endl;
	cout << "The area of the square = " << area(lengthA) << endl;
	cout << "The area of the rectangle = " << area(lengthA, lengthB) << endl;

	system("pause");
}