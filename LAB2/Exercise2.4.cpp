#include <iostream>

using namespace std;

float earnings(float pln, int hours[][4]) {
	int allHours = 0, days, weeks;
	for (weeks = 0; weeks < 4; weeks++)
	{
		for (days = 0; days < 7; days++) allHours += hours[days][weeks];
	}
	return (allHours * pln);
}


int main() 
{
	int week[7][4];
		int dayNr, weekNr;
	float money, earning;
	cout << "Monthly earings calculator.\n"
		<< "How much you earn in an hour: ";
	cin >> money;
	cout << endl;
	for (weekNr = 0; weekNr < 4; weekNr++)
	{
		cout << "Week " << weekNr + 1 << endl;

		for (dayNr = 0; dayNr < 7; dayNr++)
		{
			cout << "How many hours you plan to work on";
			if (dayNr == 0) cout << " Monday: ";
			if (dayNr == 1) cout << " Tuesday: ";
			if (dayNr == 2) cout << " Wednesday: ";
			if (dayNr == 3) cout << " Thursday: ";
			if (dayNr == 4) cout << " Friday: ";
			if (dayNr == 5) cout << " Saturday: ";
			if (dayNr == 6) cout << " Sunday: ";
			cin >> week[dayNr][weekNr];
			cout << endl;
		}
	}
	cout << endl;
	earning = earnings(money, week);
	cout << "You will earn: " << earning << " PLN" << " in four weeks\n";
		return 0;
}

