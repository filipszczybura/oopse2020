#include <iostream>
#include <fstream>
#include<string>
using namespace std;
int main()
{
	fstream file;

	file.open("lab2textfile.txt", ios::in | ios::out);
	if (file.good() == true)
	{
		cout << "File access granted!" << endl;

		file << "This text will show in the file !" << endl;
		string userInput, text;
		text = "this is a string, that will be placed in the file \n";
		cout << "Enter any string, it will be copied into the file" << endl;
		getline(cin, userInput);
		file.write(&userInput[0], userInput.length());

		//reading from file
//moving the cursor by 3 bytes towards the beginning of the file

		file.seekg(+3, std::ios_base::beg);

		cout << "First line: " << endl << userInput << endl;

		string userInput2, userInput3;

		file << userInput2;

		getline(file, userInput2);
		cout << "Next line from the file: " << endl << userInput2 << endl;

		char temp[10];
		file.read(temp, 10);

		cout << "Another line from the file: " << endl;
		
		for (int i = 0;i < 10; i++)
		{
			cout << temp[i] << endl;
		}

		file.close();
	}
	else
	{
		cout << "File access forbidden " << endl;
	}
	system("pause");
}
