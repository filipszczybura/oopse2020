#include <iostream>
#include <array>

using namespace std;

float willEarn(float price, int hours[])
{
	int allHours = 0;
	for (int i = 0; i < 7; i++)
	{
		allHours += hours[i];
	}
	return (allHours * price);
}

int main()
{
	int dayWorkTime[7];
	float money, salary;

	cout << " Weekly earrings calculator." << endl;
	cout << "How much money you earn per hour: ";
	cin >> money;
	cout << endl;

	for (int i = 0; i < 7; i++)
	{
		cout << "How many hours you plan to work on";
		if (i == 0) 
			cout << " Monday: ";
        if (i == 1) 
			cout << " Tuesday: ";
		if (i == 2) 
			cout << " Wednesday: ";
		if (i == 3) 
			cout << " Thursday: ";
		if (i == 4) 
			cout << " Friday: ";
		if (i == 5) 
			cout << " Saturday: ";
		if (i == 6) 
			cout << " Sunday: ";
		
		cin >> dayWorkTime[i];
		cout << endl;

	}
	cout << endl;

	salary = willEarn(money, dayWorkTime);
	cout << "You are going to earn: " << salary << " PLN" << endl;
	system("pause");
}

