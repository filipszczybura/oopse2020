#include <iostream>
#include <array>

using namespace std;

int main()
{
	int marks[5];
	int length = *(&marks + 1) - marks;
	// pointer shows adress of the last index , which is 4+1 = 5 , - marks which is 0 adress , it results in length of 5-0 = 5 
	int lp[5];
	float meanval = 0;
	
	marks[0] = 3;
	marks[1] = 5;
	marks[2] = 4;
	marks[3] = 3;
	marks[4] = 2;

	cout << "List of marks: " << endl;

	for (int i = 0; i < length; i++)
	{
		meanval += marks[i];
	}
	meanval /= 5;

	for (int i = 0; i < length; i++)
	{
		lp[i] = i + 1;
		cout << "\t Mark nr. " << lp[i] << ". " << marks[i] << endl;
	}
	cout << "Mean value of the marks: " << meanval << endl;

	system("pause");
}

