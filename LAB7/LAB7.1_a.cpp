#include <iostream>
#include<string>

using namespace std;

class Number
{
	int value;
	
public:

	Number(int val)
	{
		value = val;
	}

	void display()
	{
		cout << value << endl;
	}
};


int main()
{
	Number x1 = Number(5);
	Number x2(7);
	cout << "Five = " << endl;
	x1.display();
	cout << "Seven = " << endl;
	x2.display();

	system("pause");

}