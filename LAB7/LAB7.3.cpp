#include <iostream>
#include<string>

using namespace std;

class Processor
{
	int n_threads;
	int n_cores;
public:
	Processor();
	Processor(int);
	Processor(int, int);
	~Processor();

	void display();
};

Processor :: ~Processor()
{
	cout << "Obiekt zostal zniszczony" << endl;
}

Processor :: Processor()
{
	n_threads = 1;
	n_cores = 1;
}

Processor :: Processor(int a)
{
	n_threads = a;
	n_cores = a;
}

Processor :: Processor(int a, int b)
{
	n_threads = a;
	n_cores = b;
}

void Processor :: display()
{
	cout << "Procesor posiada " << n_threads << " watkow i "
		<< n_cores << " rdzeni" << endl;
}

int main()
{
	Processor i5 = Processor(4, 4);
	Processor i7 = Processor(8, 4);
	Processor p3;
	Processor c2d = Processor(2);

	cout << "Program przechowuje i wyswietla informacje na temat procesorow" << endl;
	
	cout << "p3: " << endl;
	p3.display();
	cout << "c2d: " << endl;
	c2d.display();
	cout << "i5: " << endl;
	i5.display();
	cout << "i7: " << endl;
	i7.display();
	cout << endl;
	
	system("pause");

}