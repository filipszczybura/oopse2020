#include <iostream>
#include<string>

using namespace std;

class Processor
{
	int n_threads;
	int n_cores;
public:
	Processor(int, int);
	void display();

};

Processor::Processor(int threads, int cores)
{
	n_threads = threads;
	n_cores = cores;
}

void Processor::display()
{
	cout << endl << "Procesor posiada " << n_threads << " watkow" << endl;
	cout << endl << "Procesor posiada " << n_cores << " rdzeni" << endl;
}

int main()
{
	Processor i5 = Processor(4, 4);
	Processor i7 = Processor(8, 4);

	cout << "Program przechowuje i wyswietla informacje na temat procesorow" << endl;

	cout << "i5" << endl;
	i5.display();

	cout << "i7" << endl;
	i7.display();


	system("pause");

}