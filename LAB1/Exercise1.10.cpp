#include <iostream>

using namespace std;

	float polekwadratu(float bok1, float bok2)
	{
		float wynik;
		wynik = bok1 * bok2;
		cout << "Obliczam pole...\n\n";
		return wynik;
		}

	int main()
	{
		float a, b, pole;
		cout << "Podaj dlugosc pierwszego boku: ";
		cin >> a;
		cout << endl << "Podaj dlugosc drugiego boku: ";
		cin >> b;
		cout << endl << "Twoj prostokat posiada boki o dlugosci: a = "
			<< a << ", b = " << b << endl;
		pole = polekwadratu(a, b);
		cout << "Pole Twojego prostokata wynosi: " << pole << endl;
		return 0;
	}
