#include<iostream>
#include<array>
#include<vector>
#include<math.h>

using namespace std;

int main()
{
	vector<int> v(10, 0);
	v.insert(v.begin(), 1);
	v.insert(v.end(), 3, 6);
	v.insert(v.begin(), 3, 2);
	for (auto& i : v)
	{
		cout << i << endl;
	}

	system("pause");
}