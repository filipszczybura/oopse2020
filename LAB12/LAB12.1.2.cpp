#include<iostream>
#include<array>
#include<vector>
#include<math.h>

using namespace std;

template<class T>
class Number
{
private:
	T var;
public:
	T getVar();
	T square();
	void set();
};

int main()
{
	Number<int> intVar;
	Number<double> doubleVar;

	cout << "Program will calculate the square of the int and double numbers." << endl;
	cout << "Enter integer value:" << endl;
	intVar.set();
	cout << intVar.getVar() << endl;
	cout << intVar.square() << endl;
	cout << "Enter double value:" << endl;
	doubleVar.set();
	cout << doubleVar.getVar() << endl;
	cout << doubleVar.square() << endl;
	system("pause");
}

template<class T>
void Number<T>::set()
{
	cin >> this->var;
}

template<class T>
T Number<T> :: square()
{
	return pow(this->var, 2);
}

template<class T>
T Number<T> ::getVar()
{
	return this->var;
}