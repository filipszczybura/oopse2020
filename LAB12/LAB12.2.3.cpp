#include<iostream>
#include<array>
#include<vector>
#include<math.h>

using namespace std;

int main()
{
	vector<int> v(10, 0);
	v.insert(v.begin(), 1);
	v.insert(v.end(), 3, 6);
	v.insert(v.begin(), 3, 2);
	for (auto& i : v)
	{
		cout << i << endl;
	}
	v.erase(v.begin());
	v.erase(v.begin() + 2, v.begin() + 4);
	v.erase(v.begin() + 4, v.end());

	for (auto it = v.begin(); it != v.end(); it++)
	{
		cout << *it << endl;
	}

	system("pause");
}