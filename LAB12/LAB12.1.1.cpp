#include<iostream>
#include<array>
#include<vector>
#include<math.h>

using namespace std;

template <typename T>
T print(T a)
{
	return a;
}

template<typename T>
T square(T a)
{
	return pow(a, 2);
}

int main()
{
	int first = 4;
	float second = 2.8;
	cout << "Program calculates the square of " << first
		<< "(int) and " << second << " (float) numbers with same function." << endl;
	cout << square(first) << endl;
	cout << square(second) << endl;
	system("pause");
}