#include<iostream>
#include<array>
#include<vector>
#include<math.h>

using namespace std;

template<typename T>
void desc(vector<T>& v)
{
	cout << "Size: " << v.size() << endl
		<< "Capacity: " << v.capacity() << endl;
}

int main()
{
	vector<int> v;
	for (int i = 0; i < 10; i++)
	{
		if (i == 0)
		{
			desc<int>(v);
		}
		v.push_back(i);
		desc<int>(v);
	}
	system("pause");
}