#include <iostream>
#include<string>

using namespace std;

class Processor
{
	int n_thread;
	int n_cores;

public: 
	
	int procParam(int thr, int cores)
	{
	 n_thread = thr;
	 n_cores = cores;
	 return thr, cores;
	}

	void dispParam();
};

void Processor::dispParam()
{
	cout << "Processor has " << n_thread << " threads" << endl;
	cout << "Procesor has " << n_cores << " cores" << endl;

}


int main()
{
	Processor i5, i7;
	cout << "Program stores and displays the processors "
		<< "data"<<endl;

	i5.procParam(4, 4);
	i7.procParam(8, 4);
	
	cout << "i5: " << endl;
	i5.dispParam();
	cout << "i7: " << endl;
	i7.dispParam();

	system("pause");
}