#include<iostream>
#include<array>
#include<vector>

#define MAX_DEVICE_NUMBER 15
#define MAX_SENSOR_NUMBER 10

using namespace std;

array<array<int, MAX_DEVICE_NUMBER>, MAX_SENSOR_NUMBER> detector;

bool loadEvent();
void analyzeEvent();
unsigned long downloadWord();

int main()
{
	cout << "Program analyzes the gathered data." << endl;

	while (loadEvent())
	{
		analyzeEvent();
	}

	cout << "There is no more data." << endl;

	system("pause");
}

bool loadEvent()
{
	struct Word
	{
		unsigned int current : 16;
		unsigned int device : 8;
		unsigned int sensor : 6;
		unsigned int : 2;
	};

	static union
	{
		unsigned long inches;
		Word word;
	};

	cout << "Loading the next event..." << endl;
	while (true)
	{
		inches = downloadWord();
		if (!inches)
		{
			return false;
		}

		if (word.device == 0xf8)
		{
			cout << "Koniec odczytu danych zdarzenia nr" << word.current << endl;
			return true;
		}
		else
		{
			if (word.device > MAX_DEVICE_NUMBER 
				|| word.sensor >= MAX_SENSOR_NUMBER)
			{
				continue;
			}
			detector[word.device][word.sensor] = word.current;
		}
	}
}

void analyzeEvent()
{
	cout << "Event analysis. These devices and sensors have worked properly:" << endl;

	for (int d = 0; d < MAX_DEVICE_NUMBER; d++)
	{
		for (int s = 0; s < MAX_SENSOR_NUMBER; s++)
		{
			if (detector[d][s])
			{
				cout << "\tDevice " << d << ", sensor " << s <<
					", results = " << detector[d][s] << endl;
			}
		}
	}
}

unsigned long downloadWord()
{
	vector<unsigned long> words = { 0x4060658, 0x60201ff, 0x9058c, 0xf80000,
		328457, 100729404, 0xf80001, 197827, 134417127, 84087033, 50927293,
		16848207, 17105686, 16847128, 0xf80002, 0 };
	
	static int counter;

	return words[counter++];
};