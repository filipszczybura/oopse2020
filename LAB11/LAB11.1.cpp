#include<iostream>
#include<string>
#include<array>

using namespace std;

struct Book
{
	char title[50]; // we use pointers here because this is an array
	char author[50];
	char subject[100];
	int bookID;
	void printBook();
};

void Book::printBook()
{
	cout << "Book title: " << title << endl;
	cout << "Book author: " << author << endl;
	cout << "Book subject: " << subject << endl;
	cout << "Book ID: " << bookID << endl << endl;

}

int main()
{
	Book b1;
	// The difference between strcpy and strcat is that strcpy initilizes the variable,
	// if if was not initilized before
	// If a variable was not initilized ( in structure and class we don't initilize! ),
	// then strcat will not work.
	strcpy_s(b1.title, sizeof b1.title, "Heaven's Devils");
	strcpy_s(b1.author, sizeof b1.author, "William C. Dietz");
	strcpy_s(b1.subject, sizeof b1.subject, "Starcraft novel");
	b1.bookID = 463231;

	Book b2;
	strcpy_s(b2.title, sizeof b2.title, "Misery");
	strcpy_s(b2.author, sizeof b2.author, "Stephen King");
	strcpy_s(b2.subject, sizeof b2.subject, "Psychological thriller");
	b2.bookID = 543212;

	b1.printBook();
	b2.printBook();
	
	system("pause");
}