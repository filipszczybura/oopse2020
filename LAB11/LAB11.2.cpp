#include<iostream>
#include<string>
#include<array>

using namespace std;

union Box
{
	long long int bigObj;
	char smallObj;
	float numObj;
};

static union
{
	int tmpNumber;
	char tmpChar;
	double tmpFP;
};

int main()
{
	Box power = { 9000 };
	cout << "It's over " << power.bigObj << endl;

	tmpFP = 3.14; // because of the union, we know that tmpFP is a double, we don't need to declare it there
	cout << "PI = " << tmpFP << endl;
	tmpNumber = 42;
	cout << "What is the answer to the ultimate question of life, the universe and everthing?" << endl;
	cout << tmpNumber << endl;

	system("pause");
}