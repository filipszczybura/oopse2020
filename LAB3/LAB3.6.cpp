#include <iostream>
#include<string>

using namespace std;

float srednia(float a, float tab[])
{
	float sred = 0;

	for (int n = 0; n < a; n++)
	{
		sred += tab[n];
	}
	sred /= a;
	return sred;

}

int main()
{
	int ile = 0;
	float* wsk, wynik;
	cout << "Program obliczy srednia arytmetyczna kilku liczb"<<endl;
	cout << "Podaj ile liczb chcesz wprowadzic w celu obliczenia sredniej: ";
	cin >> ile;
	wsk = new float[ile];

	for (int i = 0; i < ile; i++)
	{
		cout << "Podaj wartosc dla " << i + 1 << " elementu: ";
		cin >> wsk[i];
		cout << endl;
	}
	cout << "Policze srednia dla nastepujacych liczb: " << endl;
	for (int j = 0; j < ile; j++)
	{
		cout << wsk[j] << ", ";
	}
	cout << endl;

	wynik = srednia(ile, wsk);
	cout << "Srednia powyzszych liczb wynosi: " << wynik << endl;
	delete wsk;
	wsk = NULL;

	if (!wsk) {
		cout << "pamiec sie wyczerpala" << endl;
	}

	system("pause");
}
