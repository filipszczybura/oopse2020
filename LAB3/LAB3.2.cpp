#include <iostream>
#include<string>

using namespace std;

int main()
{
	int masa = 135, * waga;
	int wybor;
	float km = 14.96, * moc;

	void* wskaznik;
	waga = &masa;
	moc = &km;

	cout << "Dobierz motocykl o pojemnosci do 125cm^3 dla siebie." << endl
		<< "Jaki parametr jest dla Ciebie najistotniejszy?" << endl
		<< "\t 1. Masa" << endl
		<< "\t 2. Moc silnika" << endl
		<< "Wprowadz swoj wybor: " << endl;
	cin >> wybor;

	if (wybor == 1)
	{
		wskaznik = waga;
		cout << "Motocykl dla Ciebie to: Yamaha Virago 125" << endl
			<< "Jego masa to: " << *(int*)wskaznik << " kg" << endl;
	}
	if (wybor == 2) 
	{
		wskaznik = moc;
		cout << "Motocykl dla Ciebie to: Honda VT Shadow 125\n"
			<< "Jego moc to: " << *(float*)wskaznik << "KM" << endl;
	}

	system("pause");

}
