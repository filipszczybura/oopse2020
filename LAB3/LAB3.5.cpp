#include <iostream>
#include<string>

using namespace std;

char *makeObject()
{
	char* wsk;
	cout << endl << "Tworze obiekt!" << endl;
	wsk = new char;
	return wsk;
}

int main()
{
	char* wsk1, * wsk2, * wsk3;
	cout << "Teraz nastapi akt stworzenia!" << endl;
	wsk1 = makeObject();
	wsk2 = makeObject();
	wsk3 = makeObject();

	*wsk1 = 'a';
	*wsk2 = 'b';
	cout << "\nStworzylismy trzy nowe obiekty.\n"
		<< "Do dwoch przypisano wartosci: " << *wsk1 << *wsk2 << endl
		<< "W trzecim widzimy same smieci: " << *wsk3 << endl;
	delete wsk1, wsk2, wsk3;
	system("pause");
}
