#include <iostream>


using namespace std;

class Character
{
	int str, dex, vit, wis, cha;
public:
	
	void assignPoints(int add)
	{
		str = add;
		dex = add;
		vit = add;
		wis = add;
		cha = add;
	}
	
	void dispPoints()
	{
		cout << "\t Hero stats:" << endl;
		cout << "Strength: " << str << endl;
		cout << "Dexterity: " << dex << endl;
		cout << "Wisdom: " << wis << endl;
		cout << "Vitality: " << vit << endl;
		cout << "Charisma: " << cha << endl;
	}
};

class Knight : public Character
{ 

public:
	Knight(int add)
	{
		assignPoints(add);
		dispPoints();
	}

};

int main()
{
	int point = 0;

	do
	{
		if (point > 9)
		{
			cout << "Learn to read! the number supposed to be from 0 to 9" << endl;
		}
			cout << "Knight is being created..." << endl;
			cout << "Enter a number from 0 to 9: ";
			cin >> point;
	} while (point > 9); // odwrotny warunek jest przy do while
	//chcemy powtarza� t� p�tl� tak d�ugo, a� kto� nie poda liczby mniejszej ni� 9
	
	//oznacza to, wykonuj p�tl� za ka�dym razem , gdy kto� poda liczb� wi�ksz� ni� 9

	Knight k1 = Knight(point);


	system("pause");
}